var target = "";
var tmp_target = "";
var target_arr = [];
var in_chatroom = true;
var user_email = "";
var user_namearr = [];
var user_name = "";
var database_name = "";
var chatroomcnt;
var chatroomindex = -1;
var tmppp = -1;
var tmp_name = "";
var chatroomname = "";
var last = null;
var mode = 0; //1 for group 2 for undo
var cursor_flag = 0;
var kickname;
var time_flag = 1;
function init() {
  firebase.auth().onAuthStateChanged(function (user) {
    var menu = document.getElementById("dynamic-menu");
    // Check user login
    if (user) {
      user_email = user.email;
      for (var i = 0; i < user_email.length; i++) {
        if (user_email[i] != ".") user_namearr.push(user_email[i]);
        else {
          user_namearr.push("@");
        }
      }
      user_name = user_namearr.join("");
      menu.innerHTML =
        "<span class='dropdown-item'>" +
        user.email +
        "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
      var logoutbtn = document.getElementById("logout-btn");
      logoutbtn.addEventListener(
        "click",
        function () {
          firebase
            .auth()
            .signOut()
            .then(function () {
              alert("User sign out success!");
              document.getElementById("contenttitle").innerHTML = "";
              window.location.href = "signin.html";
            })
            .catch(function (error) {
              ("User sign out failed!");
              document.getElementById("contenttitle").innerHTML = "";
            });
        },
        false
      );
      if (chatroomindex != -1) {
        get_content();
      }
      get_chatroom();
    } else {
      window.location.href = "signin.html";
      window;
      user_name = "";
      menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
      document.getElementById("userlist").innerHTML = "";
      document.getElementById("content_list").innerHTML = "";
    }
  });
  getnameright();

  inv_btn = document.getElementById("addfriend");
  inv_btn.addEventListener("click", function () {
    if (mode == 0) {
      alert("Choose a room first!");
    } else {
      var croom = prompt("想邀請誰呢?", "");

      if (croom == null || croom == "") {
      } else {
        var friend_email = croom;
        var friend_emailarr = [];
        var friend_id;
        for (var i = 0; i < friend_email.length; i++) {
          if (friend_email[i] != ".") friend_emailarr.push(friend_email[i]);
          else {
            friend_emailarr.push("@");
          }
        }
        friend_id = friend_emailarr.join("");
        var today = new Date();
        var currentDateTime =
          today.getFullYear() * 10000000000 +
          (today.getMonth() + 1) * 100000000 +
          today.getDate() * 1000000 +
          today.getHours() * 10000 +
          today.getMinutes() * 100 +
          today.getSeconds();
        var data = {
          name: chatroomname,
          number: chatroomindex,
          time: 0 - currentDateTime,
          cnt: 0,
        };
        var tmppref = firebase.database().ref("/child/" + friend_id);
        var push = tmppref.push(data);

        var memberdata = {
          user: friend_email,
          visibility: 1,
        };
        var memberref = firebase
          .database()
          .ref("/child/" + chatroomindex + "-user");
        memberref.push(memberdata);
      }
    }
  });

  add_btn = document.getElementById("addchatroom");
  add_btn.addEventListener("click", function () {
    var user = firebase.auth().currentUser;
    if (user != null) {
      var croom = prompt("請輸入聊天室名稱", "ChatRoom");

      if (croom == null || croom == "") {
      } else {
        var today = new Date();
        var currentDateTime =
          today.getFullYear() * 10000000000 +
          (today.getMonth() + 1) * 100000000 +
          today.getDate() * 1000000 +
          today.getHours() * 10000 +
          today.getMinutes() * 100 +
          today.getSeconds();
        currentDateTime = 0 - currentDateTime;
        chatroomcnt++;
        var data = {
          name: croom,
          number: chatroomcnt,
          time: currentDateTime,
          cnt: 0,
        };
        var cntdata = {
          num: chatroomcnt,
        };

        var user = firebase.auth().currentUser;
        if (user) {
          var ref = firebase.database().ref("/child/" + "cnt");
          ref.set(cntdata);
          var Ref = firebase.database().ref("/child/" + user_name);
          Ref.push(data);
        }
        var memberdata = {
          user: user_email,
          visibility: 1,
        };
        var memberref = firebase
          .database()
          .ref("/child/" + chatroomcnt + "-user");
        memberref.push(memberdata);
        var commentcnt = {
          cnt: 0,
        };
        var commentcntref = firebase
          .database()
          .ref("/child/" + chatroomcnt + "@@commentcnt");
        commentcntref.update(commentcnt);
      }
    }
  });

  post_btn = document.getElementById("send");
  post_txt = document.getElementById("text");

  post_btn.addEventListener("click", function () {
    if (mode != 0) {
      if (post_txt.value != "") {
        var today = new Date();
        var currentDateTime =
          today.getFullYear() +
          "/" +
          (today.getMonth() + 1) +
          "/" +
          today.getDate() +
          "/(" +
          (today.getHours() < 10 ? "0" : "") +
          today.getHours() +
          ":" +
          (today.getMinutes() < 10 ? "0" : "") +
          today.getMinutes() +
          ")";
        var txt = html_encode(post_txt.value);
        var data = {
          data: txt,
          email: user_email,
          time: currentDateTime,
        };
        var user = firebase.auth().currentUser;
        if (user) {
          database_name = chatroomindex + "@@chat";
          var Ref = firebase.database().ref("/child/" + database_name);
          Ref.push(data);
          post_txt.value = "";
          var cmtcntref = firebase
            .database()
            .ref("/child/" + chatroomindex + "@@commentcnt");
          var cmtcnt;
          cmtcntref
            .once("value")
            .then(function (snapshot) {
              cmtcnt = snapshot.val().cnt;
              cmtcnt++;
              var dataa = {
                cnt: cmtcnt,
              };
              cmtcntref.set(dataa);
              time_flag = 0;
            })
            .then(function (e) {
              updatetime(user_email);
            })
            .then(function (k) {
              time_flag = 1;
            });
        }
      }
    }
  });
}

function get_chatroom() {
  var postsRef = firebase.database().ref("/child/" + user_name);
  var total_post = [];
  postsRef
    .orderByChild("time")
    .once("value")
    .then(function (snapshot) {
      console.log("DDDDDDDDDDDDDDD");
      snapshot.forEach(function (childshot) {
        var childData = childshot.val();
        var cmtcount;
        tmppp = childData.number;
        tmp_name = childData.name;
        var memberRef = firebase
          .database()
          .ref("/child/" + childData.number + "@@commentcnt");
        memberRef.once("value").then(function (snapshott) {
          cmtcount = snapshott.val().cnt;
          var color =
            childData.number == chatroomindex
              ? "lightgray"
              : snapshott.val().cnt > childData.cnt
              ? "#ff6666"
              : "darkgray";

          var str_before_username =
            "<div style='background:" +
            color +
            "' class='my-3 p-3 rounded box-shadow' id='chatroom" +
            childData.number +
            "' onClick = 'changechat(this," +
            childData.number +
            ");' >";

          total_post[total_post.length] =
            str_before_username + childData.name + "</div> \n";
        });
      });

      document.getElementById("userlist").innerHTML = total_post.join("");
      postsRef.orderByChild("time").on("value", function (snapshotr) {
        if (time_flag == 1) {
          total_post = [];
          var cnt = 0;
          snapshotr.forEach(function (dataa) {
            cnt++;
            var childDataa = dataa.val();
            console.log("THERE");
            console.log(childDataa.name);
            var cmtcount;
            tmppp = childDataa.number;
            tmp_name = childDataa.name;
            var memberRef = firebase
              .database()
              .ref("/child/" + childDataa.number + "@@commentcnt");
            memberRef
              .once("value")
              .then(function (snapshott) {
                cmtcount = snapshott.val().cnt;
                var color =
                  childDataa.number == chatroomindex
                    ? "lightgray"
                    : snapshott.val().cnt > childDataa.cnt
                    ? "#ff6666"
                    : "darkgray";

                var str_before_username =
                  "<div style='background:" +
                  color +
                  "' class='my-3 p-3 rounded box-shadow' id='chatroom" +
                  childDataa.number +
                  "' onClick = 'changechat(this," +
                  childDataa.number +
                  ");' >";

                total_post[total_post.length] =
                  str_before_username + childDataa.name + "</div> \n";
              })
              .then(function (hi) {
                document.getElementById("userlist").innerHTML = total_post.join(
                  ""
                );
              });
          });
          if (cnt == 0) document.getElementById("userlist").innerHTML = "";
        }
      });
    })
    .catch((e) => console.log(e.message));
}
function updatetime(user_email) {
  console.log("start");
  var chatroomidx = chatroomindex;
  var memberref = firebase.database().ref("/child/" + chatroomidx + "-user");
  memberref.once("value").then(function (snapshot) {
    snapshot.forEach(function (childshot) {
      if (memberref.key == chatroomidx + "-user") {
        var childData = childshot.val();
        var membername = childData.user;
        var member_arr = [];
        var member_email = membername;
        var member_name = ""; //ice@allen
        for (var i = 0; i < member_email.length; i++) {
          if (member_email[i] != ".") member_arr.push(member_email[i]);
          else {
            member_arr.push("@");
          }
        }
        member_name = member_arr.join("");
        var new_cnt;
        var memberRef = firebase.database().ref("/child/" + member_name);
        memberRef.once("value").then(function (snapshott) {
          snapshott.forEach(function (childshott) {
            var memchildData = childshott.val();
            var key = childshott.key;
            if (memchildData.number == chatroomidx) {
              var today = new Date();
              var currentDateTime =
                today.getFullYear() * 10000000000 +
                (today.getMonth() + 1) * 100000000 +
                today.getDate() * 1000000 +
                today.getHours() * 10000 +
                today.getMinutes() * 100 +
                today.getSeconds();
              currentDateTime = 0 - currentDateTime;

              var cmtcnt;
              var cmtcntref = firebase
                .database()
                .ref("/child/" + chatroomidx + "@@commentcnt");
              cmtcntref.once("value").then(function (snapshot) {
                new_cnt = memchildData.cnt + 1;
                cmtcnt =
                  user_email == membername
                    ? memchildData.cnt + 1
                    : memchildData.cnt;
                var data = {
                  name: memchildData.name,
                  number: memchildData.number,
                  time: currentDateTime,
                  cnt: cmtcnt,
                };

                var tmpref = firebase
                  .database()
                  .ref("/child/" + member_name + "/" + key);
                tmpref.update(data);
                console.log("stop");
              });
            }
          });
        });
      }
    });
  });
}

function get_content() {
  var ref;
  ref = firebase.database().ref("/child/" + chatroomindex + "@@chat");

  // List for store posts html
  var total_post = [];
  // Counter for checking history post update complete
  var first_count = 0;
  // Counter for checking when to update new post
  var second_count = 0;
  if (mode == 0) {
  } else if (mode == 1) {
    ref
      .once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childshot) {
          var childData = childshot.val();
          var str_before_username =
            childData.email == user_email
              ? "<div class='my-3 p-3 bg-blue rounded box-shadow' id='my_text'><div class='media text-dark pt-1'>"
              : "<div class='my-3 p-3 bg-light rounded box-shadow' id='your_text' ><div class='media text-dark pt-1'>";

          var str_before_username_2 =
            "</div><div class='media text-dark pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-black'><strong>";
          var str_after_content = "</p></div></div>\n";
          total_post[total_post.length] =
            str_before_username +
            childData.time +
            str_before_username_2 +
            childData.email +
            "</strong><br>" +
            childData.data +
            str_after_content;
          first_count += 1;
          var cmtcntref = firebase
            .database()
            .ref("/child/" + chatroomindex + "@@commentcnt");
          var cmtcnt;
          cmtcntref.once("value").then(function (snapshot) {
            cmtcnt = snapshot.val().cnt;
            var userRef = firebase.database().ref("/child/" + user_name);
            var tmpname;
            var tmpnumber;
            var tmptime;
            var key;
            userRef.once("value").then(function (snapshot) {
              snapshot.forEach(function (childshott) {
                childdata = childshott.val();
                key = childshott.key;
                if (childdata.number == chatroomindex) {
                  tmpname = childdata.name;
                  tmpnumber = childdata.number;
                  tmptime = childdata.time;
                  var dat = {
                    name: tmpname,
                    number: tmpnumber,
                    time: tmptime,
                    cnt: cmtcnt,
                  };
                  var tmpref3 = firebase
                    .database()
                    .ref("/child/" + user_name + "/" + key);
                  tmpref3.update(dat);
                }
              });
            });
          });
        });
        scrollToEnd();
        document.getElementById("content_list").innerHTML = total_post.join("");

        ref.on("child_added", function (data) {
          if (ref.key == chatroomindex + "@@chat" && mode == 1) {
            second_count += 1;
            var childData = data.val();
            if (second_count > first_count) {
              var str_before_username =
                childData.email == user_email
                  ? "<div class='my-3 p-3 bg-blue rounded box-shadow' id='my_text'><div class='media text-dark pt-1'>"
                  : "<div class='my-3 p-3 bg-light rounded box-shadow' id='your_text' ><div class='media text-dark pt-1'>";

              var str_before_username_2 =
                "</div><div class='media text-dark pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-black'><strong>";
              var str_after_content = "</p></div></div>\n";
              total_post[total_post.length] =
                str_before_username +
                childData.time +
                str_before_username_2 +
                childData.email +
                "</strong><br>" +
                childData.data +
                str_after_content;
              document.getElementById(
                "content_list"
              ).innerHTML = total_post.join("");

              var cmtcntref = firebase
                .database()
                .ref("/child/" + chatroomindex + "@@commentcnt");
              var cmtcnt;
              cmtcntref.once("value").then(function (snapshot) {
                cmtcnt = snapshot.val().cnt;
                var userRef = firebase.database().ref("/child/" + user_name);
                var tmpname;
                var tmpnumber;
                var tmptime;
                var key;
                userRef.once("value").then(function (snapshot) {
                  snapshot.forEach(function (childshott) {
                    childdata = childshott.val();
                    key = childshott.key;
                    if (childdata.number == chatroomindex) {
                      tmpname = childdata.name;
                      tmpnumber = childdata.number;
                      tmptime = childdata.time;
                      var dat = {
                        name: tmpname,
                        number: tmpnumber,
                        time: tmptime,
                        cnt: cmtcnt,
                      };
                      var tmpref3 = firebase
                        .database()
                        .ref("/child/" + user_name + "/" + key);
                      tmpref3.update(dat);
                    }
                  });
                });
              });
            }
          }
          scrollToEnd();
        });
      })
      .catch((e) => console.log(e.message));
  }
}
function deleteall() {
  var tar = firebase.database().ref("/child/");
  var data = {
    int: "HI",
  };
  tar.set(data);
}
function scrollToEnd() {
  var scroll = document.getElementById("content_list");
  scroll.scrollTop = scroll.scrollHeight;
}
function html_encode(str) {
  var s = "";
  if (str.length == 0) return "";
  s = str.replace(/&/g, "&amp;");
  s = s.replace(/</g, "&lt;");
  s = s.replace(/>/g, "&gt;");
  s = s.replace(/ /g, "&nbsp;");
  s = s.replace(/\'/g, "&apos;");
  s = s.replace(/\"/g, "&quot;");
  s = s.replace(/\n/g, "<br>");
  return s;
}

function getnameright() {
  var user = firebase.auth().currentUser;
  if (user != null) {
    user_email = user.email;
    for (var i = 0; i < user_email.length; i++) {
      if (user_email[i] != ".") user_namearr.push(user_email[i]);
      else {
        user_namearr.push("@");
      }
    }
    user_name = user_namearr.join("");
  } else {
  }
}
window.onload = function () {
  document.getElementById("membersbtn").style.visibility =
    mode == 0 ? "hidden" : "visible";
  document.getElementById("deletebtn").style.visibility =
    mode == 2 ? "visible" : "hidden";
  getnameright();
  loadcnt();
  init();
};
function changechatclr() {
  var ref = firebase.database().ref("/child/" + user_name);
  ref.once("value").then(function (snapshot) {
    snapshot.forEach(function (childshot) {
      var childData = childshot.val();
      var doc = document.getElementById("chatroom" + childData.number);
      doc.style.background =
        childData.number == chatroomindex
          ? "lightgray"
          : doc.style.background == "#ff6666"
          ? "#ff6666"
          : "darkgray";
    });
  });
}
async function changechat(e, tmp) {
  if (mode != 0) {
    mode = 1;
    changebtnimg();
  } else mode = 1;
  document.getElementById("membersbtn").style.visibility =
    mode == 0 ? "hidden" : "visible";

  await changeindex(tmp);
  document.getElementById("deletebtn").style.visibility =
    mode == 2 ? "visible" : "hidden";
  changechatclr();
  document.getElementById("contenttitle").innerHTML = e.innerHTML;
  changename(e.innerHTML);
  await get_content();
  cursor_flag = 0;

  document.body.style.cursor = "default";
}

function changebtnimg() {
  document.getElementById("deletebtn").style.visibility =
    mode == 2 ? "visible" : "hidden";
  var btn = document.getElementById("btnimg");
  var commentbar = document.getElementById("contenttitle");
  if (mode == 1) {
    btn.src = "./group.png";

    get_content();
    commentbar.innerHTML = chatroomname;
  } else if (mode == 2) {
    btn.src = "./undo.png";
    commentbar.innerHTML = "MEMBERS";
    get_userlist();
  }
}
function changebtnimgg() {
  cursor_flag = 0;

  document.body.style.cursor = "default";
  if (mode == 1) {
    mode = 2;
  } else if (mode == 2) {
    mode = 1;
  }
  changebtnimg();
}
function get_userlist() {
  if (mode == 2) {
    var indexnow = chatroomindex;
    var ref;
    ref = firebase.database().ref("/child/" + indexnow + "-user");

    // List for store posts html
    var total_post = [];

    ref
      .once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childshot) {
          var childData = childshot.val();
          kickname = childData.user;
          var str_before_username =
            "<div class='my-3 w-25 h-10 p-3 bg-light rounded box-shadow' id ='memberinroom' onClick='kicknumber(this);'>";

          total_post[total_post.length] =
            str_before_username + childData.user + "</div> \n";
        });

        if (mode != 0)
          document.getElementById("content_list").innerHTML = total_post.join(
            ""
          );
      })
      .catch((e) => console.log(e.message));
  }
}
function kick() {
  cursor_flag = cursor_flag == 0 ? 1 : 0;

  document.body.style.cursor =
    cursor_flag == 1 ? "url('./kick.png')0 30 ,default" : "default";
}
async function kicknumber(e) {
  var nowindex = chatroomindex;
  if (cursor_flag == 1) {
    var yes = confirm("Kick " + e.innerHTML + "?");
    if (yes) {
      var ref = firebase.database().ref("/child/" + nowindex + "-user");
      ref.once("value").then(function (snapshot) {
        snapshot.forEach(function (childshot) {
          var childData = childshot.val();
          if (childData.user == e.innerHTML) {
            var key = childshot.key;
            var tmpref = firebase
              .database()
              .ref("/child/" + nowindex + "-user/" + key);
            tmpref.remove();
          }
        });
      });
      var kickuser_arr = [];
      var kickuser_email = e.innerHTML;
      var kickuser_name = "";
      for (var i = 0; i < kickuser_email.length; i++) {
        if (kickuser_email[i] != ".") kickuser_arr.push(kickuser_email[i]);
        else {
          kickuser_arr.push("@");
        }
      }
      kickuser_name = kickuser_arr.join("");
      var userref = firebase.database().ref("/child/" + kickuser_name);
      await userref.once("value").then(function (snapshot) {
        snapshot.forEach(function (childshot) {
          var childData2 = childshot.val();
          if (childData2.number == nowindex) {
            var key2 = childshot.key;
            var tmpref2 = firebase
              .database()
              .ref("/child/" + kickuser_name + "/" + key2);
            tmpref2.remove();
          }
        });
      });
      var gogo = await get_userlist();
      await kickmyself(kickuser_name);
    }
  }
}
function kickmyself(kickuser_name) {
  if (kickuser_name == user_name) {
    chatroomindex = -1;
    mode = 0;
    cursor_flag = 0;
    document.getElementById("contenttitle").innerHTML = "";
    document.getElementById("content_list").innerHTML = "";
    document.body.style.cursor = "default";
    document.getElementById("membersbtn").style.visibility = "hidden";
    document.getElementById("deletebtn").style.visibility = "hidden";

    var btn = document.getElementById("btnimg");

    btn.src = "./group.png";
  }
}
function changeindex(index) {
  chatroomindex = index;
}
function changename(name) {
  chatroomname = name;
}
function loadcnt() {
  var cntref;
  cntref = firebase.database().ref("/child/" + "cnt");
  cntref.on("value", function (snapshot) {
    var childData = snapshot.val();
    if (childData == null) {
      var data = {
        num: 0,
      };
      cntref.set(data);
      chatroomcnt = 0;
    } else if (childData.num == NaN) {
      var data = {
        num: 0,
      };
      cntref.set(data);
      chatroomcnt = 0;
    } else {
      chatroomcnt = childData.num;
    }
  });
}
