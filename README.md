# Software Studio 2021 Spring Midterm Project

## Topic

- Project Name : Chatroom

## Basic Components

|      Component       | Score | Y/N |
| :------------------: | :---: | :-: |
| Membership Mechanism |  15%  |  Y  |
|    Firebase Page     |  5%   |  Y  |
|       Database       |  15%  |  Y  |
|         RWD          |  15%  |  Y  |
|  Topic Key Function  |  20%  |  Y  |

## Advanced Components

|      Component      | Score | Y/N |
| :-----------------: | :---: | :-: |
| Third-Party Sign In | 2.5%  |  Y  |
| Chrome Notification |  5%   |  N  |
|  Use CSS Animation  | 2.5%  |  Y  |

# 作品網址：

https://midterm-chatroom-108062129.web.app/index.html

## Website Detail Description

A private chat room that can only be invited to access a room.

# Components Description :

## 1. Membership Mechanism:

It's similiar to Lab6, everyone who want to use this app should register an account on this app. Or simply log in by Google.

![](https://i.imgur.com/rP6bJO2.png)

## 2. Firebase Page:

Use firebase deploy.
https://midterm-chatroom-108062129.web.app/index.html

## 3. Database

Use realtime database to handle data.

![](https://i.imgur.com/MelYN5s.png)

And the database is only accessible for those who are authenticated.

![](https://i.imgur.com/bhjCfN3.png)

## 4. RWD

Use css to make the website RWD, it's hard to demostrate in README so I think I will show this at DEMO.
Basically my website will shrink it's size to fit in different window.

## 5. Topic Key Function

Like every Chatroom app, you can chat with others in this room. However, the only way to enter a chatroom is to be invited by the members of the room.

![](https://i.imgur.com/PZR95Kt.png)

Use these two buttons below to control chatroom, "+" means adding a new chatroom while "invite" means inviting others into current chatroom.

![](https://i.imgur.com/b4Yazu0.png)

![](https://i.imgur.com/JRU1ZSo.png)

![](https://i.imgur.com/nBhEUOr.png)

## 6. Third-Party Sign In

Included in Membership Mechanism mentioned above.

## 7. Use CSS Animation

By keyframe, we can let objects in html move or change to reach animation. It's also hard to demostrate in README so I think I will show this part at DEMO.
The background of signin page is sand floating.

## 8. Security Report

By encoding the data text before sending out, we will tell the program that these are just text but not programmable html code.

![](https://i.imgur.com/mJVBgqz.png)

# Other Functions Description :

## 1. Who Is In The Room?

By clicking the button below, your will see the list that is in this room.

![](https://i.imgur.com/U2UA5rk.png)

![](https://i.imgur.com/olTpyFZ.png)

What's more, click the kick icon and the cursor will become the same as that icon, and then you can kick every members in this room, including yourself.

![](https://i.imgur.com/hDXDGQF.png)

## 2. You Got New Message!

There will be three type of colors for the chatroom list.
Darkgray represents normal chatroom, Lightgray represents which chatroom you are , and Red one represents that there're some new message you haven't read in this room!

![](https://i.imgur.com/iO2d2d4.png)

The order of the chatroom list is decided by the latest message that has been sent in this chatroom just like Line or Messenger.
